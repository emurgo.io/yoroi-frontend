// @flow

import type { LocatorObject } from '../support/webdriver';

export const walletNameInput: LocatorObject = {
  locator: 'walletNameInput',
  method: 'id',
};
export const walletPasswordInput: LocatorObject = {
  locator: 'walletPasswordInput',
  method: 'id',
};
export const repeatPasswordInput: LocatorObject = {
  locator: 'repeatPasswordInput',
  method: 'id',
};
export const newWalletDialogPlate: LocatorObject = {
  locator: 'walletPlateText',
  method: 'id',
};
